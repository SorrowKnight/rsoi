﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RSOI1.Startup))]
namespace RSOI1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
