﻿using System;
using System.Web.Mvc;
using OAuth;
using System.IO;
using NLog;
using System.Net;
using Evernote.EDAM.UserStore;
using Thrift.Protocol;
using Thrift.Transport;
using Evernote.EDAM.NoteStore;
using Evernote.EDAM.Type;
using System.Web;

namespace RSOI1.Controllers
{
    public class HomeController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        
        public static void Log(string message)
        {
            var log = new System.IO.StreamWriter("log.txt", true);
            log.WriteLine(message);
            log.Close();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult WorkDatAccessToken()
        {

            try
            { 
                var urli = this.Url.Action();
                Log("Authorize return is " + urli);
            
                verifier = urli.Substring(urli.IndexOf("oauth_verifier="), urli.IndexOf("&sandbox") - urli.IndexOf("oauth_verifier="));
                Log("Verifier is " + verifier);

                var uri = new Uri("http://sandbox.evernote.com/oauth");
                
                string requestUrl = uri + "?" + "&oauth_consumer_key=" + consumerKey + "&" + reqToken + 
                    "&" + verifier +
                    "&" + "oauth_nonce=" + nonce + "&oauth_signature_method=" + "PLAINTEXT" +
                    "&oauth_signature=" + consumerSecret +
                    "&" + "oauth_timestamp=" + timeStamp + "&" +
                    "oauth_version=" + "1.0";
                Log(requestUrl);
                var request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                using (var sr = new StreamReader(((HttpWebResponse)request.GetResponse()).GetResponseStream()))
                {
                    var response = sr.ReadToEnd();
                    Log("Access response is " + response);
                    accToken = Server.UrlDecode(response.Substring(response.IndexOf("oauth_token=") + 12,
                                                response.IndexOf("&") - response.IndexOf("oauth_token=") - 12));
                    noteStoreAdress = Server.UrlDecode(response.Substring(response.IndexOf("edam_noteStoreUrl") + 18,
                                                        response.IndexOf("&edam_webApiUrl") - response.IndexOf("edam_noteStoreUrl") - 18));
                }
                Log("Access token is " + accToken);
                return View("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "С токеном доступа не сложилось";
                return View("Message");
            }
        }

        public ActionResult Access()
        {
            var uri = new Uri("http://sandbox.evernote.com/oauth");
            consumerKey = "hosk192-8294";
            consumerSecret = "ed033d95636ffaa0";

            OAuthRequest client = new OAuthRequest
            {
                Method = "GET",
                Type = OAuthRequestType.RequestToken,
                SignatureMethod = OAuthSignatureMethod.HmacSha1,
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                RequestUrl = "https://sandbox.evernote.com/oauth",
                Version = "1.0",
                CallbackUrl = "http://localhost:55073/Home/WorkDatAccessToken"
            };
            string auth = client.GetAuthorizationQuery();
            var url = client.RequestUrl + "?" + auth;
            Log("url is " + url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            try
            { 
                //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                using (var sr = new StreamReader(((HttpWebResponse)request.GetResponse()).GetResponseStream()))
                {
                    var response = sr.ReadToEnd();
                    reqToken = response.Substring(response.IndexOf("oauth_token="), response.IndexOf("&") - response.IndexOf("oauth_token="));
                }
                Log("Request token is " + reqToken);

                request = (HttpWebRequest)HttpWebRequest.Create("http://sandbox.evernote.com/OAuth.action?" + reqToken);
                request.AllowAutoRedirect = false;
                HttpWebResponse authorize = (HttpWebResponse)request.GetResponse();

                return new RedirectResult(authorize.ResponseUri.AbsoluteUri);
            }
            catch (Exception ex)
            { 
                    ViewBag.Message = "Авторизация попортилась";
                    return View("Message");
            }


        }

        public ActionResult MeAction()
        {
            try
            {
                UserStore store = new UserStore();
                THttpClient userStoreHttpClient = new THttpClient(new Uri("https://sandbox.evernote.com/edam/user"));
                var netWorks = new Classes.EvernoteWorks(new TBinaryProtocol(userStoreHttpClient));
                var pooser = netWorks.GetPooserInfo(accToken);
                Log(pooser);
                ViewBag.Message = pooser;
                return View("Message");
            }
            catch (Evernote.EDAM.Error.EDAMUserException ex)
            {
                ViewBag.Message = "Не удалось получить информацию о пользователе";
                return View("Message");
            }
        }

        public ActionResult SomeRequest()
        {
            try
            { 
                string nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml.dtd\">";
                nBody += "<en-note>Hello world</en-note>";
                THttpClient userStoreHttpClient = new THttpClient(new Uri(noteStoreAdress));
                var note = new Classes.Note (){ Title = "New note", Content = nBody };
                var netWorks = new Classes.EvernoteWorks(new TBinaryProtocol(userStoreHttpClient));
                netWorks.createNote(accToken, note);

                ViewBag.Message = "Гляньте аккаунт";
                return View("Message");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Не удалось создать запись";
                return View("Message");
            }
        }

        private static string reqToken;
        private static string accToken;
        private static string consumerKey;
        private static string consumerSecret;
        private static string nonce;
        private static string sig;
        private static string verifier;
        private static string timeStamp;
        private static string noteStoreAdress;
    }
}