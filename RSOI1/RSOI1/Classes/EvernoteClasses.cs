﻿using Evernote.EDAM.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Thrift.Protocol;

namespace RSOI1.Classes
{


    [System.Serializable]
    public class GetUserResult : TBase
    {
        public Isset __isset;
        private User _success;
        private EDAMSystemException _systemException;
        private EDAMUserException _userException;

        public void Read(TProtocol iprot)
        {
            TField field;
            iprot.ReadStructBegin();
        Label_0007:
            field = iprot.ReadFieldBegin();
            if (field.Type == TType.Stop)
            {
                iprot.ReadStructEnd();
            }
            else
            {
                switch (field.ID)
                {
                    case 0:
                        if (field.Type != TType.Struct)
                        {
                            TProtocolUtil.Skip(iprot, field.Type);
                            break;
                        }
                        this.Success = new User();
                        this.Success.Read(iprot);
                        break;

                    case 1:
                        if (field.Type != TType.Struct)
                        {
                            TProtocolUtil.Skip(iprot, field.Type);
                            break;
                        }
                        this.UserException = new EDAMUserException();
                        this.UserException.Read(iprot);
                        break;

                    case 2:
                        if (field.Type != TType.Struct)
                        {
                            TProtocolUtil.Skip(iprot, field.Type);
                            break;
                        }
                        this.SystemException = new EDAMSystemException();
                        this.SystemException.Read(iprot);
                        break;

                    default:
                        TProtocolUtil.Skip(iprot, field.Type);
                        break;
                }
                iprot.ReadFieldEnd();
                goto Label_0007;
            }
        }

        public override string ToString()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder("getUser_result(");
            builder.Append("Success: ");
            builder.Append((this.Success == null) ? ((string)"<null>") : this.Success.ToString());
            builder.Append(",UserException: ");
            builder.Append((this.UserException == null) ? ((string)"<null>") : this.UserException.ToString());
            builder.Append(",SystemException: ");
            builder.Append((this.SystemException == null) ? ((string)"<null>") : this.SystemException.ToString());
            builder.Append(")");
            return builder.ToString();
        }

        public void Write(TProtocol oprot)
        {
            TStruct struc = new TStruct("getUser_result");
            oprot.WriteStructBegin(struc);
            TField field = new TField();
            if (this.__isset.success)
            {
                if (this.Success != null)
                {
                    field.Name = "Success";
                    field.Type = TType.Struct;
                    field.ID = 0;
                    oprot.WriteFieldBegin(field);
                    this.Success.Write(oprot);
                    oprot.WriteFieldEnd();
                }
            }
            else if (this.__isset.userException)
            {
                if (this.UserException != null)
                {
                    field.Name = "UserException";
                    field.Type = TType.Struct;
                    field.ID = 1;
                    oprot.WriteFieldBegin(field);
                    this.UserException.Write(oprot);
                    oprot.WriteFieldEnd();
                }
            }
            else if (this.__isset.systemException && (this.SystemException != null))
            {
                field.Name = "SystemException";
                field.Type = TType.Struct;
                field.ID = 2;
                oprot.WriteFieldBegin(field);
                this.SystemException.Write(oprot);
                oprot.WriteFieldEnd();
            }
            oprot.WriteFieldStop();
            oprot.WriteStructEnd();
        }

        public User Success
        {
            get
            {
                return this._success;
            }
            set
            {
                this.__isset.success = true;
                this._success = value;
            }
        }

        public EDAMSystemException SystemException
        {
            get
            {
                return this._systemException;
            }
            set
            {
                this.__isset.systemException = true;
                this._systemException = value;
            }
        }

        public EDAMUserException UserException
        {
            get
            {
                return this._userException;
            }
            set
            {
                this.__isset.userException = true;
                this._userException = value;
            }
        }

        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct Isset
        {
            public bool success;
            public bool userException;
            public bool systemException;
        }
    }

    [System.Serializable]
    public class GetUserArgs : TBase
    {
        public Isset __isset;
        private string _authenticationToken;

        public void Read(TProtocol iprot)
        {
            iprot.ReadStructBegin();
            while (true)
            {
                TField field = iprot.ReadFieldBegin();
                if (field.Type == TType.Stop)
                {
                    break;
                }
                if (field.ID == 1)
                {
                    if (field.Type == TType.String)
                    {
                        this.AuthenticationToken = iprot.ReadString();
                    }
                    else
                    {
                        TProtocolUtil.Skip(iprot, field.Type);
                    }
                }
                else
                {
                    TProtocolUtil.Skip(iprot, field.Type);
                }
                iprot.ReadFieldEnd();
            }
            iprot.ReadStructEnd();
        }

        public override string ToString()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder("getUser_args(");
            builder.Append("AuthenticationToken: ");
            builder.Append(this.AuthenticationToken);
            builder.Append(")");
            return builder.ToString();
        }

        public void Write(TProtocol oprot)
        {
            TStruct struc = new TStruct("getUser_args");
            oprot.WriteStructBegin(struc);
            TField field = new TField();
            if ((this.AuthenticationToken != null) && this.__isset.authenticationToken)
            {
                field.Name = "authenticationToken";
                field.Type = TType.String;
                field.ID = 1;
                oprot.WriteFieldBegin(field);
                oprot.WriteString(this.AuthenticationToken);
                oprot.WriteFieldEnd();
            }
            oprot.WriteFieldStop();
            oprot.WriteStructEnd();
        }

        public string AuthenticationToken
        {
            get
            {
                return this._authenticationToken;
            }
            set
            {
                this.__isset.authenticationToken = true;
                this._authenticationToken = value;
            }
        }

        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct Isset
        {
            public bool authenticationToken;
        }
    }

    [System.Serializable]
    public class CreateNoteArgs : TBase
    {
        public Isset __isset;
        private string _authenticationToken;
        private Note _note;

        public void Read(TProtocol iprot)
        {
            TField field;
            iprot.ReadStructBegin();
        Label_0007:
            field = iprot.ReadFieldBegin();
            if (field.Type == TType.Stop)
            {
                iprot.ReadStructEnd();
            }
            else
            {
                switch (field.ID)
                {
                    case 1:
                        if (field.Type != TType.String)
                        {
                            TProtocolUtil.Skip(iprot, field.Type);
                            break;
                        }
                        this.AuthenticationToken = iprot.ReadString();
                        break;

                    case 2:
                        if (field.Type != TType.Struct)
                        {
                            TProtocolUtil.Skip(iprot, field.Type);
                            break;
                        }
                        this.Note = new Note();
                        this.Note.Read(iprot);
                        break;

                    default:
                        TProtocolUtil.Skip(iprot, field.Type);
                        break;
                }
                iprot.ReadFieldEnd();
                goto Label_0007;
            }
        }

        public override string ToString()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder("createNote_args(");
            builder.Append("AuthenticationToken: ");
            builder.Append(this.AuthenticationToken);
            builder.Append(",Note: ");
            builder.Append((this.Note == null) ? ((string)"<null>") : this.Note.ToString());
            builder.Append(")");
            return builder.ToString();
        }

        public void Write(TProtocol oprot)
        {
            TStruct struc = new TStruct("createNote_args");
            oprot.WriteStructBegin(struc);
            TField field = new TField();
            if ((this.AuthenticationToken != null) && this.__isset.authenticationToken)
            {
                field.Name = "authenticationToken";
                field.Type = TType.String;
                field.ID = 1;
                oprot.WriteFieldBegin(field);
                oprot.WriteString(this.AuthenticationToken);
                oprot.WriteFieldEnd();
            }
            if ((this.Note != null) && this.__isset.note)
            {
                field.Name = "note";
                field.Type = TType.Struct;
                field.ID = 2;
                oprot.WriteFieldBegin(field);
                this.Note.Write(oprot);
                oprot.WriteFieldEnd();
            }
            oprot.WriteFieldStop();
            oprot.WriteStructEnd();
        }

        public string AuthenticationToken
        {
            get
            {
                return
                  this._authenticationToken;
            }
            set
                {
                this.__isset.authenticationToken = true;
                this._authenticationToken = value;
            }
            }


            public Note Note
        {
            get
            {
                return
                  this._note;
            }
            set
                {
                this.__isset.note = true;
                this._note = value;
            }
            }

            [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
            public struct Isset
        {
            public bool authenticationToken;
            public bool note;
        }
    }


}