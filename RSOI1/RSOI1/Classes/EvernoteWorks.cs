﻿using Evernote.EDAM.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Thrift.Protocol;

namespace RSOI1.Classes
{
    public class EvernoteWorks
    {
        protected TProtocol iprot_;
        protected TProtocol oprot_;

        public EvernoteWorks(TProtocol prot)
        {
            this.iprot_ = prot;
            this.oprot_ = prot;
        }

        public string GetPooserInfo(string authToken)
        {
            this.oprot_.WriteMessageBegin(new TMessage("getUser", TMessageType.Call, 1));
            
            new GetUserArgs { AuthenticationToken = authToken }.Write(this.oprot_);
            this.oprot_.WriteMessageEnd();
            this.oprot_.Transport.Flush();

            if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
            {
                this.iprot_.ReadMessageEnd();
                throw new Exception();
            }
            GetUserResult _result = new GetUserResult();
            _result.Read(this.iprot_);
            this.iprot_.ReadMessageEnd();
            if (_result.__isset.success)
            {
                return _result.Success.ToString();
            }
            if (_result.__isset.userException)
            {
                throw _result.UserException;
            }
            if (_result.__isset.systemException)
            {
                throw _result.SystemException;
            }
            throw new Exception("getUser failed: unknown result");
        }

        public void createNote(string authenticationToken, Note note)
        {
            this.oprot_.WriteMessageBegin(new TMessage("createNote", TMessageType.Call, 1));
            new CreateNoteArgs()
            {
                AuthenticationToken = authenticationToken,
                Note = note
            }.Write(this.oprot_);
            this.oprot_.WriteMessageEnd();
            this.oprot_.Transport.Flush();
        }

    }
}