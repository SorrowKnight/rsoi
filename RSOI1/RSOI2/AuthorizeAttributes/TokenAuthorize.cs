﻿using RSOI2Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSOI2Client.AuthorizeAttributes
{
    public class TokenAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            var authHeader = httpContext.Request.Headers["Authorization"];
            var accessToken = authHeader.Substring(authHeader.IndexOf("Bearer ") + 7, authHeader.Length - authHeader.IndexOf("Bearer "));
            if (!IsTokened(accessToken))
            {
                return false;
            }
            return true;
            
        }

        private bool IsTokened(string token)
        {
            try
            {
                using (var db = new ContractsModel())
                { 
                    var tokenInDb = db.TokenStorage.First(x => x.AccessToken == token);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}