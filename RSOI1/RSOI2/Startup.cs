﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RSOI2Client.Startup))]
namespace RSOI2Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
