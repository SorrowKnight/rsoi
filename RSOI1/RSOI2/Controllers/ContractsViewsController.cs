﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSOI2Client.Models;
using PagedList;
using System.IO;
using RSOI2Client.AuthorizeAttributes;

namespace RSOI2Client.Controllers
{
    public class ContractsViewsController : Controller
    {
        private ContractsModel db = new ContractsModel();

        // GET: ContractsViews
        public ActionResult Index(int? page)
        {
            int pageSize = 2;
            int pageNumber = (page ?? 1);
            return View(db.ContractsView.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: ContractsViews/Details/5
        public ActionResult Details(int id)
        {
            ContractsView contractsView = db.ContractsView.FirstOrDefault(i => i.Id == id);
            if (contractsView == null)
            {
                return HttpNotFound();
            }
            return View(contractsView);
        }

        // GET: ContractsViews/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.ExistingUsers = new SelectList(db.AspNetUsers.ToList().Select(p => p.Email));
            ViewBag.PropertyTypes = new SelectList(db.PropertyType.ToList().Select(p => p.PropertyName));
            return View();
        }

        // POST: ContractsViews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(ContractsView contractsView)
        {
            if (ModelState.IsValid)
            {
                string mail = Request.Form["ExistingUsers"].ToString();
                string propertyName = Request.Form["PropertyTypes"].ToString();
                var userId = db.AspNetUsers.FirstOrDefault(i => i.Email == mail).Id;
                var propertyTypeId = db.PropertyType.FirstOrDefault(i => i.PropertyName == propertyName).Id;
                var contract = new Contracts() { Number = contractsView.Number, UserId = userId, PropertyTypeId = propertyTypeId };
                db.Contracts.Add(contract);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contractsView);
        }

        // GET: ContractsViews/Edit/5
        [TokenAuthorize]
        public ActionResult Edit(int id)
        {
            ContractsView contractsView = db.ContractsView.FirstOrDefault(i => i.Id == id);
            if (System.Web.HttpContext.Current.User.Identity.Name != contractsView.Email)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (contractsView == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExistingUsers = new SelectList(db.AspNetUsers.ToList().Select(p => p.Email), db.AspNetUsers.ToList().FirstOrDefault(x => x.Email == contractsView.Email).Email);
            ViewBag.PropertyTypes = new SelectList(db.PropertyType.ToList().Select(p => p.PropertyName), db.PropertyType.ToList().FirstOrDefault(p => p.PropertyName == contractsView.PropertyName).PropertyName);
            return View(contractsView);
        }

        // POST: ContractsViews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TokenAuthorize]
        public ActionResult Edit(ContractsView contractsView)
        {
            if (ModelState.IsValid)
            {
                string mail = Request.Form["ExistingUsers"].ToString();
                string propertyName = Request.Form["PropertyTypes"].ToString();
                var userId = db.AspNetUsers.FirstOrDefault(i => i.Email == mail).Id;
                var propertyTypeId = db.PropertyType.FirstOrDefault(i => i.PropertyName == propertyName).Id;
                var contract = db.Contracts.FirstOrDefault(i => i.Id == contractsView.Id);
                contract.UserId = userId;
                contract.PropertyTypeId = propertyTypeId;
                contract.Number = contractsView.Number;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contractsView);
        }

        // GET: ContractsViews/Delete/5
        [TokenAuthorize]
        public ActionResult Delete(int id)
        {
            ContractsView contractsView = db.ContractsView.FirstOrDefault(i => i.Id == id);
            if (System.Web.HttpContext.Current.User.Identity.Name != contractsView.Email)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (contractsView == null)
            {
                return HttpNotFound();
            }
            return View(contractsView);
        }

        // POST: ContractsViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [TokenAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            db.DeleteContract(id);
            return RedirectToAction("Index");
        }


        private bool ValidateToken (Token token)
        {
            try
            {
                var tokenInDb = db.TokenStorage.First(x => x.AccessToken == token.AccessToken);
                if ((tokenInDb.CreationDate - DateTime.Now).Seconds >= token.ExpiresIn)
                {
                    if (token.RefreshToken == token.AccessToken + "derp")
                    {
                        var req = HttpWebRequest.Create(this.Url.Action() + "GiveAccessToken");
                        using (var sr = new StreamReader(((HttpWebResponse)req.GetResponse()).GetResponseStream()))
                        {
                            var response = req.GetResponse();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
