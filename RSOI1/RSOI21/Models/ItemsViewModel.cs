namespace RSOI21.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ItemsViewModel : DbContext
    {
        public ItemsViewModel()
            : base("name=ItemsViewModel")
        {
        }

        public virtual DbSet<ItemsView> ItemsView { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemsView>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<ItemsView>()
                .Property(e => e.Number)
                .IsFixedLength();
        }
    }
}
