namespace RSOI21.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TokenStorage")]
    public partial class TokenStorage
    {
        public int Id { get; set; }

        [Required]
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
