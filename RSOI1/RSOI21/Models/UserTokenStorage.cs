namespace RSOI21.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserTokenStorage")]
    public partial class UserTokenStorage
    {
        public int Id { get; set; }

        [Required]
        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public int? ExpiresIn { get; set; }

        public string RefreshToken { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}
