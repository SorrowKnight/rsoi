namespace RSOI21.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Items
    {
        public int Id { get; set; }

        public int? ContractId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual Contracts Contracts { get; set; }
    }
}
