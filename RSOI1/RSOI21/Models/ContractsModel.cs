namespace RSOI21.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContractsModel : DbContext
    {
        public ContractsModel()
            : base("name=ContractsModel")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<PropertyType> PropertyType { get; set; }
        public virtual DbSet<TokenStorage> TokenStorage { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<ContractsView> ContractsView { get; set; }
        public virtual DbSet<ItemsView> ItemsView { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Contracts)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contracts>()
                .Property(e => e.Number)
                .IsFixedLength();

            modelBuilder.Entity<Contracts>()
                .HasMany(e => e.Items)
                .WithOptional(e => e.Contracts)
                .HasForeignKey(e => e.ContractId);

            modelBuilder.Entity<Items>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<ContractsView>()
                .Property(e => e.Number)
                .IsFixedLength();

            modelBuilder.Entity<ItemsView>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<ItemsView>()
                .Property(e => e.Number)
                .IsFixedLength();
        }

        internal void DeleteContract(int id)
        {
            var items = this.Items.Where(x => x.ContractId == id);
            this.Items.RemoveRange(items);
            this.Contracts.Remove(this.Contracts.FirstOrDefault(x => x.Id == id));
            this.SaveChanges();
        }
    }
}
