﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RSOI21.Startup))]
namespace RSOI21
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
