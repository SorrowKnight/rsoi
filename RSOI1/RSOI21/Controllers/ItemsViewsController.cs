﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSOI21.Models;
using PagedList;

namespace RSOI21.Controllers
{
    public class ItemsViewsController : Controller
    {
        private ContractsModel db = new ContractsModel();

        // GET: ItemsViews
        public ActionResult Index(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(db.ItemsView.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: ItemsViews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsView itemsView = db.ItemsView.Find(id);
            if (itemsView == null)
            {
                return HttpNotFound();
            }
            return View(itemsView);
        }

        // GET: ItemsViews/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.ExistingContracts = new SelectList(db.Contracts.ToList().Select(p => p.Number));
            return View();
        }

        // POST: ItemsViews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(ItemsView itemsView)
        {
            if (ModelState.IsValid)
            {
                string number = Request.Form["ExistingContracts"].ToString();
                var numberId = db.Contracts.FirstOrDefault(x => x.Number == number).Id;
                db.Items.Add(new Items() { Name = itemsView.Name, ContractId = numberId});
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(itemsView);
        }

        // GET: ItemsViews/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsView itemsView = db.ItemsView.Find(id);
            var contract = db.ContractsView.FirstOrDefault(x => x.Number == itemsView.Number);
            if (System.Web.HttpContext.Current.User.Identity.Name != contract.Email)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (itemsView == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExistingContracts = new SelectList(db.Contracts.ToList().Select(p => p.Number), db.Contracts.ToList().FirstOrDefault(p => p.Number == itemsView.Number).Number);
            return View(itemsView);
        }

        // POST: ItemsViews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ItemsView itemsView)
        {
            if (ModelState.IsValid)
            {
                string number = Request.Form["ExistingContracts"].ToString();
                var numberId = db.Contracts.FirstOrDefault(x => x.Number == number).Id;
                var item = db.Items.FirstOrDefault(x => x.Id == itemsView.Id);
                item.Name = itemsView.Name;
                item.ContractId = numberId;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemsView);
        }

        // GET: ItemsViews/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsView itemsView = db.ItemsView.Find(id);
            var contract = db.ContractsView.FirstOrDefault(x => x.Number == itemsView.Number);
            if (System.Web.HttpContext.Current.User.Identity.Name != contract.Email)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (itemsView == null)
            {
                return HttpNotFound();
            }
            return View(itemsView);
        }

        // POST: ItemsViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var item = db.Items.FirstOrDefault(i => i.Id == id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
