﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSOI21.Models;
using PagedList;


namespace RSOI21.Controllers
{
    public class UsersController : Controller
    {
        private ContractsModel db = new ContractsModel();

        // GET: Users
        public ActionResult Index(int? page)
        {
            int pageSize = 2;
            int pageNumber = (page ?? 1);
            return View(db.AspNetUsers.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.AspNetUsers.FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Details/5
        // [Authorize]
        public ActionResult Me()
        {
            var user = System.Web.HttpContext.Current.User;
            var id = db.AspNetUsers.FirstOrDefault(x => x.Email == user.Identity.Name).Id;
            return RedirectToAction("Details", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
